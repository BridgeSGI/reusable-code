export SRC_HOME=/opt/teradyne/build-svn/sterling-gold-svn
export INSTALL_DIR=/opt/teradyne/ssfsgold/Foundation

# Commands to SVN Checkout
cd /opt/teradyne/build-svn
rm -rf sterling-gold-svn/
svn co http://spwpscanengine1.icd.teradyne.com/subVersion/sterling sterling-gold-svn

echo "Starting EAR build......."
#To run build from /opt/teradyne/build-svn/sterling-gold-svn/build/scripts:-
#cd $SRC_HOME/build/scripts
#ant ear

#To run build from /opt/teradyne/ssfsgold/Foundation/bin:
cd $INSTALL_DIR/bin
ant ear

# To List the newly created EAR file with size and date
ls -l $INSTALL_DIR/external_deployments
